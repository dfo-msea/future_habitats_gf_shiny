# Future Habitats Groundfish Shiny App

__Main author:__  Sarah Davies  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Pacific Biological Station  
__Contact:__      e-mail: Sarah.Davies@dfo-mpo.gc.ca  | tel:


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
  + [Subsections within contents](#subsections-within-contents)
- [Methods](#methods)
  + [Subsections within methods](#subsections-within-methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Create a shiny app to display supplementary figures for the following publication:

Thompson, P.L., Nephin, J., Davies, S.C., Park, A.E., Lyons, D.A., Rooper, C.N., Peña, M.A., Christian, J.R., Hunter, K.L., Rubidge, E. and Holdsworth, A.M. 2022. Groundfish biodiversity change in northeastern Pacific waters under projected warming and deoxygenation. bioRxiv: 2022.2005.2004.490690. 10.1101/2022.05.04.490690.

